var zoomed = document.getElementsByClassName('zoom');
for (var i = 0; i < zoomed.length; i++) {
    zoomed[i].addEventListener('click', openModal);
};

function openModal() {
    document.getElementById("modal").style.display = "flex";
    document.getElementById("modal").firstElementChild.src = this.src;
    document.getElementById("modal").addEventListener("click", function() {
        document.getElementById("modal").style.display = "none";
    });
};

// document.addEventListener("DOMContentLoaded", function() {
//   var demo = document.querySelector("main");
//   demo.scrollIntoView({ behavior: 'smooth', duration: 1000 });
// });